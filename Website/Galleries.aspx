<%@ Page Language="C#" MasterPageFile="~/MasterPageDefault.master" AutoEventWireup="true" CodeFile="Galleries.aspx.cs" Inherits="_Default" Title="Dean Komen Photography" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script language="javascript" type="text/javascript">

     function SetImage(imageId, imageSource, categoryName, categoryElementID)
    {
        var imageElement = document.getElementById(imageId);
        imageElement.src=imageSource;        
        SetCatagory(categoryElementID, categoryName)
    }
    function NavigateTo(url)
    {
        window.location.href = url;
    }
    
    function SetCatagory(categoryElementID, catagoryName)
    {
        var catagoryElement = document.getElementById(categoryElementID);
        if(catagoryName!='')
        {
            catagoryElement.innerHTML= catagoryName;        
        }
        else
        {
            catagoryElement.innerHTML= "select a gallery above";        
        }
    }
    
    if (document.images) 
    {
        img1 = new Image();
        img2 = new Image();
        img3 = new Image();
        img4 = new Image();
        img5 = new Image();
        
        img1.src = "~/Images/MirrorOccasions.jpg";
        img2.src = "~/Images/MirrorLandscape.jpg"
        img3.src = "~/Images/MirrorNature.jpg"
        img4.src = "~/Images/MirrorArt.jpg"
        img5.src = "~/Images/WildlifeFineArt.jpg"
        
        img6 = new Image();
        img7 = new Image();
        img8 = new Image();
        img9 = new Image();
        img10 = new Image();
        img11 = new Image();
        img7.src = "~/Images/SubMenu/N1.jpg"
        img9.src = "~/Images/SubMenu/Sodwana.jpg"
        img10.src = "~/Images/SubMenu/Desktop.jpg"
    }

    
</script>
    <BR />
    <BR />
    <BR />
    <table style="width:750px;position:relative;top:-10px;margin-left: auto;margin-right: auto;" border="0">
        <tr>
            <td style="height:300px;width:3px;background-color:#777777"></td>
            <td valign='top'>
                <table border='0' cellpadding='10' cellspacing='0'>
                    <tr><td style="height:30px"></td></tr>
                    <tr>
                        <td>
                            &nbsp;
                            <asp:image runat="server" id="MirrorWildlifePortfolio" ImageUrl="~/Images/MirrorWildlifePortfolioOut.jpg" ToolTip=""/>
                            &nbsp;
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                            <asp:image runat="server" id="MirrorLandscape" ImageUrl="~/Images/MirrorLandscapeOut.jpg" ToolTip=""/>
                            &nbsp;
                            &nbsp;
                        </td>
                        <td>
                            <asp:image runat="server" id="MirrorNature" ImageUrl="~/Images/MirrorNatureOut.jpg" ToolTip=""/>
                        </td>
                        <%--<td>
                            <asp:image runat="server" id="SchizoidReality" ImageUrl="~/Images/SchizoidRealityOut.jpg" ToolTip=""/>
                        </td>--%>
                    </tr>
                    <tr>
                        <td style="height:15px;background-color:#000000" colspan='4'>
                            <asp:Label style="font-size:3;color:#555555" ID="uiCatagory" runat="server" Text="main galleries"></asp:Label>
                        </td>
                    </tr>
                    <tr><td style="height:10px">
                        </td></tr>
                </table>
                <div style='display:none;font-family:Arial, Helvetica, sans-serif;font-size:10pt;font-style:italic;overflow:hidden;max-height:500px; width:900px'>              
                  The forest is haunted, aswarm with mysts of longing<br />
                  The howling of ruinous decision sweeps through me, tugging at my soul<br />
                  My bones are chilled, I am cold, brittle to the touch                      <br />
                  Loneliness has become my anthem of self-disgust                                  <br />
                  Soulless cavities, of once bright eyes, stare longingly, searchingly                   <br />
                  They scream tears from the dark and no-one can hear<br />
                  <br />
                  My rue is condemnation, desolation and loathing contempt<br />
                  Scathing dead frost my only friend                                    <br />
                  I am tested by a relentless and deep desire                                <br />
                  But the fleetings of a beautiful reality have surely passed                     <br />
                  A barren heart is my despair                                                         <br />
                  I know my fate, and no-one will care <br />                               
                  <br />  
                  Remember your days of golden warmth            <br />
                  The feelings of love, devotion and beautiful lust<br />
                  Cherish those memories for they are armour     <br />
                  Your only manna... and tether... to beauties past <br /> 
                  <br />                                                         
                  You too may visit the haunted forest, the ferry is cheap and the river is smooth <br />
                  Your mantra of loneliness may too be close <br />
                  And once you have arrived... the gates will close   <br />
                  You too will wonder, searching for relief    <br />
                  And when you settle down, realising your fate...<br />
                  <br />
                  ...remember, that I too... am crying there.    <br />
                  <br />
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Dean Komen 2013           
                </div>
            </td>
        </tr>
    </table>
    <%--<table style="width:825px;position:relative;top:-10px;margin-left: auto;margin-right: auto;" border="0">
        <tr>
            <td align='right' style="width:400px"><hr style="background-color:Olive;color:Olive;"/>
                <asp:Label style="font-size:3;color:#555555" ID="uiAdditionalGal" runat="server" Text="additional galleries"></asp:Label>
            </td>
            <td align="left">
                <table border="0" cellspacing="20px">
                    <tr>
                        <td align="center" valign='top'>
                            <asp:image runat="server" id="HumanBody" ImageUrl="Images/SubMenu/HumanBodyOut.jpg" ToolTip=""/><br />
                            <asp:Label ID="Label3" Font-Size="Small" ForeColor="LightGray" Text="Human Body" runat="server" />
                        </td>
                        <td align="center" valign='top'>
                            <asp:image runat="server" id="N1" ImageUrl="Images/SubMenu/N1Out.jpg" ToolTip=""/><br />
                            <asp:Label ID="Label5" Font-Size="Small" ForeColor="LightGray" Text="N1<BR />highway" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>--%>
    <br />
    

<%--<img width="0" visible="false" height="0" border="0" alt="" src="http://mycounter.tinycounter.com/index.php?user=dkomen" />--%>
</asp:Content>

