using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for PageTable
/// </summary>
public class PageTable
{
    #region Fields
    private string _pageName;
    private string _thumbNailsTableHTML;
    #endregion
    #region Constructors
    public PageTable(string pageName, string thumbnailsTableHTML)
	{
        _pageName = pageName;
        _thumbNailsTableHTML = thumbnailsTableHTML;
	}
    #endregion
    #region Properties
    public string PageName
    {
        get
        {
            return _pageName;
        }
        set
        {
            _pageName = value;
        }
    }
    public string ThumbnailsTableHTML
    {
        get
        {
            return _thumbNailsTableHTML;
        }
        set
        {
            _thumbNailsTableHTML = value;
        }
    }
    #endregion
}
