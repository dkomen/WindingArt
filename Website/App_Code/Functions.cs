using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Functions
/// </summary>
public static class GeneralFunctions
{
    #region Public Functions
    public static string GetGeneratedFileName(string pathToImageFile)
    {
        return (System.IO.Path.ChangeExtension(System.IO.Path.GetFileNameWithoutExtension(pathToImageFile),"aspx"));
    }
    /// <summary>
    /// Calculates the offset value required to centre a child within a parent
    /// </summary>
    /// <param name="parentLength"></param>
    /// <param name="childLength"></param>
    /// <example>
    /// parent  : **********
    /// child   : ****
    /// return value = ****** / 2 = ***
    /// Therefore   : **********
    ///                  ****
    /// </example>
    /// <returns></returns>
    public static int CalculateForCentre(int parentLength, int childLength)
    {
        return (parentLength - childLength) / 2;
    }

    /// <summary>
    /// Gets the width and height of an image on disk
    /// </summary>
    /// <param name="pathToImage"></param>
    /// <returns></returns>
    public static System.Drawing.Size GetImageSize(string pathToImage)
    {
        try
        {
            System.Drawing.Bitmap image = new System.Drawing.Bitmap(pathToImage);
            return image.Size;
        }
        catch (Exception ex)
        {
            throw new Exception("Path " + pathToImage);
        }
    }

    public static string CopyrightInfo()
    {
        return "<BR /><center><asp:Label ID='lblCopyrightInfo' runat='server' Font-Bold='false' Text='All content � Dean Komen 2007, 2008. All rights reserved' Font-Size='7' ForeColor='#626262'></asp:Label></center>";
    }
    #endregion
}
