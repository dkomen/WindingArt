using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for BaseLargeImagePage
/// </summary>
public class BaseLargeImagePage : System.Web.UI.Page
{
	public BaseLargeImagePage()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string GeneratePage(Page currentPage)
    {
	PageFunctions.DoPageHitCounter(currentPage);
        currentPage.Cache.Insert("p", "d", null, System.DateTime.UtcNow, System.Web.Caching.Cache.NoSlidingExpiration);
        string unencodedFirstFileName = string.Empty;
        string unencodedNextFileName = string.Empty;
        if (Request.QueryString["prev"] != null)
        {
            unencodedFirstFileName = System.Text.ASCIIEncoding.ASCII.GetString(System.Convert.FromBase64String(Request.QueryString["prev"]));
        }
        string unencodedCurrentFileName = System.Text.ASCIIEncoding.ASCII.GetString(System.Convert.FromBase64String(Request.QueryString["current"]));
        if (Request.QueryString["next"] != null)
        {
            unencodedNextFileName = System.Text.ASCIIEncoding.ASCII.GetString(System.Convert.FromBase64String(Request.QueryString["next"]));
        }
        GenerateLinkPage pageHtml = new GenerateLinkPage();
        string absolutePath = currentPage.MapPath(currentPage.Request.AppRelativeCurrentExecutionFilePath);
        System.Drawing.Size imageSize = new System.Drawing.Size(0, 0);
        if (Session["IgnorePageHits"] == null)
        {
            PageFunctions.PageHitCounter(currentPage.Request, unencodedCurrentFileName);
        }
        if (currentPage.Request.AppRelativeCurrentExecutionFilePath.ToLower().IndexOf("zoomify.aspx")==-1)
        {
            imageSize = GeneralFunctions.GetImageSize(Server.MapPath(unencodedCurrentFileName));
        }
        else
        {
            imageSize = GeneralFunctions.GetImageSize(Server.MapPath(System.IO.Path.GetFileName(unencodedCurrentFileName)));
            unencodedCurrentFileName = System.IO.Path.GetFileName(unencodedCurrentFileName);
        }
        
        
        return pageHtml.GeneratePageHtml(absolutePath, currentPage.Request.AppRelativeCurrentExecutionFilePath, unencodedFirstFileName, unencodedCurrentFileName, unencodedNextFileName, 1024, imageSize) + "<BR />" + GeneralFunctions.CopyrightInfo();
    }
}
