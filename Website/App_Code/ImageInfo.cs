﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Summary description for ImageInfo
/// </summary>
public class ImageInfo
{
    #region Fields
    private string _imageFileName = string.Empty;
    private string _xmlFileWithInfo = string.Empty;
    private System.Drawing.Point _imagePixelDimensions = new System.Drawing.Point();
    private System.Drawing.PointF _imageCentimeterDimensions = new System.Drawing.PointF();
    private float _framedPrice = 0;
    private string _caption = string.Empty;
    private string _secondCaption = string.Empty;
    private string _frameCaption = string.Empty;
    private string _printPaperType = string.Empty;
    #endregion
    #region Constructors
    public ImageInfo(string imageFileName, string xmlFileWithInfo)
    {
        //Set fields
        ImageFileName = imageFileName;
        XmlFileWithInfo = xmlFileWithInfo;

        //Load the xml document
        System.Xml.XmlDocument xmlDocument = new System.Xml.XmlDocument();
        xmlDocument.Load(xmlFileWithInfo);

        //Search for the required xml node entry in the document
        System.Xml.XmlNode imageNode = xmlDocument.SelectSingleNode("/galleryInfo/images/image[@filename='" + imageFileName.ToUpper() + "']");

        //Set remaining fields
        if (imageNode != null)
        {
            Caption = imageNode.Attributes["caption"].Value;
            SecondCaption = imageNode.Attributes["secondCaption"].Value;
            FrameCaption = imageNode.Attributes["frameCaption"].Value;
            FramedPrice = float.Parse(imageNode.Attributes["framedPrice"].Value);
            PrintPaperType = imageNode.Attributes["printPaperType"].Value;
            ImagePixelDimensions = new System.Drawing.Point(int.Parse(imageNode.Attributes["width"].Value), int.Parse(imageNode.Attributes["height"].Value));
            float ppi = float.Parse(imageNode.Attributes["ppi"].Value);
            float x = (float)Math.Round(((ImagePixelDimensions.X / ppi) * 2.54),0);
            float y = (float)Math.Round(((ImagePixelDimensions.Y / ppi) * 2.54),0);
            ImageCentimeterDimensions = new System.Drawing.PointF(x,y);

        }
    }
    #endregion
    #region Properties
    public string ImageFileName { get { return _imageFileName; } set { _imageFileName = value; } }
    public string XmlFileWithInfo { get { return _xmlFileWithInfo; } set { _xmlFileWithInfo = value; } }
    public System.Drawing.Point ImagePixelDimensions { get { return _imagePixelDimensions; } set { _imagePixelDimensions = value; } }
    public System.Drawing.PointF ImageCentimeterDimensions { get { return _imageCentimeterDimensions; } set { _imageCentimeterDimensions = value; } }
    public float FramedPrice { get { return _framedPrice; } set { _framedPrice = value; } }
    public string Caption { get { return _caption; } set { _caption = value; } }
    public string SecondCaption { get { return _secondCaption; } set { _secondCaption = value; } }
    public string FrameCaption { get { return _frameCaption; } set { _frameCaption = value; } }
    public string PrintPaperType { get { return _printPaperType; } set { _printPaperType = value; } }
    #endregion
}
