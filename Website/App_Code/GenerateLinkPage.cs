using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for GenerateLinkPage
/// </summary>
public class GenerateLinkPage
{
    #region Fields
    private enum PaperSize
    {
        A6 = 0, A5 = 1, A4 = 2, A3 = 3, A2 = 4, A1 = 5, A0 = 6, Undefined = 10
    }
    //private string _pathToImageToDisplay;
    //private string _fileNameForGeneratedFile;
    private double _scaleFactor1 = 1.25;
    private double _scaleFactor2 = 1.45;
    private double _standardPrintedImageCostScaleA = 550;
    private double _standardPrintedImageCostScaleB = 700;
    private int _noPrice = -1;
    #endregion
    #region Constructors
    public GenerateLinkPage()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion
    #region Public Methods
    public string GeneratePageHtml(string absolutePath, string currentGallery, string pathToPrevImageFile, string pathToCurrentImageFile, string pathToNextImageFile, int maxHeight, System.Drawing.Size imageSize)
    {

        if (imageSize.Height > maxHeight)
        {
            imageSize.Width = (int)(imageSize.Width * ((Single)maxHeight / (Single)imageSize.Height));
            imageSize.Height = maxHeight;
        }
        
        string zoomifyData = ImageIsZoomifyImage(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(absolutePath), pathToCurrentImageFile));
        if (zoomifyData != string.Empty)
        {
            if (absolutePath.ToLower().IndexOf("zoomify.aspx")!=-1)
            {
                zoomifyData = zoomifyData.Insert(6, " ALIGN='CENTER' BORDERCOLOR=#333333");
                zoomifyData = zoomifyData.Replace("BORDER=0","BORDER=1");
                zoomifyData = zoomifyData.Replace("800", (imageSize.Width+50).ToString());
                zoomifyData = zoomifyData.Replace("675", (imageSize.Height+50).ToString());
                return "<table style='position:relative;top:-10px;margin-left: auto;margin-right: auto;'><tr><td>" + "<center>" + zoomifyData + GetImageTitle(absolutePath, currentGallery, pathToCurrentImageFile) + "</center></td></tr></table>";
            }
            else
            {
                return ""; 
            }
        }
        else
        {
            System.Text.StringBuilder pageHtmlData = new System.Text.StringBuilder();
            pageHtmlData.AppendLine("<center>");
            pageHtmlData.AppendLine("   <table style='border-style:solid;border-width:2px;border-spacing:0px;border-color:#1a1a1a;padding:0px;color:gray;border-collapse:collapse;'>");
            pageHtmlData.AppendLine("       <tr>");
            pageHtmlData.AppendLine("           <td style='width:" + (imageSize.Width + 50) + "px;height:" + (imageSize.Height + 50) + "px;background-color:#1f1f1f'>");
            pageHtmlData.AppendLine("               <div style='vertical-align:middle;margin:25px;'>");
            //pageHtmlData.AppendLine("                   <div style='position:relative;height:" + imageSize.Height + "px;width:" + imageSize.Width + "px;background-color:#555555';>");
            pageHtmlData.AppendLine("                       <img title='Click for previous page' border=1 width='" + imageSize.Width + "px' height='" + imageSize.Height + "px' src='" + pathToCurrentImageFile + "' style='border-right: gray thin solid; border-top: gray thin solid; border-left: gray thin solid; border-bottom: gray thin solid' OnClick='history.go( -1 );return true;' alt='Click to go back' />");
            string imageTitle = GetImageTitle(absolutePath, currentGallery, pathToCurrentImageFile);
            if (imageTitle != string.Empty)
            {
                pageHtmlData.AppendLine("                       <center><b><br/>" + GetImageTitle(absolutePath, currentGallery, pathToCurrentImageFile) + "</b></center>");
            }
            //pageHtmlData.AppendLine("                   </div>");
            pageHtmlData.AppendLine("               </div>");
            pageHtmlData.AppendLine("           </td>");
            pageHtmlData.AppendLine("       </tr>");
            pageHtmlData.AppendLine("   </table>");
            pageHtmlData.AppendLine("   <br />");
            pageHtmlData.AppendLine("   <br />");

            pageHtmlData.AppendLine("   <a OnClick='history.go( -1 );return true;' >Click for previous page</a>");
            pageHtmlData.AppendLine("<br /><br /><img ID='uxCalibrateImage' src='http://www.windingart.com/Images/Calibration.jpg' alt='calibration image'/>");
            pageHtmlData.AppendLine("</center>");
            return pageHtmlData.ToString();
        }
        
    }
    #endregion

    private string ImageIsZoomifyImage(string pathToImageFile)
    {
        string data = string.Empty;
        string testPathForZoomify = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(pathToImageFile), System.IO.Path.GetFileNameWithoutExtension(pathToImageFile) + ".html");
        if (System.IO.File.Exists(testPathForZoomify))
        {
            System.IO.TextReader reader = System.IO.File.OpenText(testPathForZoomify);

            data = reader.ReadToEnd();
            int startOfTable = data.ToLower().IndexOf("<table");
            startOfTable = data.ToLower().IndexOf("<table", startOfTable + 1);
            int endOfTable = data.ToLower().IndexOf("</table>", startOfTable);
            data = data.Substring(startOfTable,(endOfTable - startOfTable) + "</table>".Length);
        }

        return data;
    }
    private ImageInfo GetImageInfo(string imageFileName, string xmlFileWithInfo)
    {
        try
        {
            return new ImageInfo(imageFileName.Substring(imageFileName.IndexOf("/") + 1), xmlFileWithInfo);
        }
        catch (Exception Ex)
        {
            return null;
        }
    }
    private string GetImageTitle(string absolutePath, string currentGaller, string fileName)
    {
        string encodedImageName = System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(fileName.Substring(6)));
        string textToReturn = string.Empty;

        string fileSaleData = string.Empty;

        ImageInfo imageInfo = GetImageInfo(fileName,System.IO.Path.Combine(System.IO.Path.GetDirectoryName(absolutePath),"info.xml"));

        if (imageInfo != null)
        {
            if (imageInfo.Caption.Trim() != string.Empty)
            {
                fileSaleData = imageInfo.Caption;
            }
            if (imageInfo.FramedPrice != 0) fileSaleData = fileSaleData + "<br />Print price ZAR R" + imageInfo.FramedPrice + "</ br>";
            if (imageInfo.ImageCentimeterDimensions != null) fileSaleData = fileSaleData + "<br /><font size='1'>Print\\Paper size : " + imageInfo.ImageCentimeterDimensions.X + "cm x " + imageInfo.ImageCentimeterDimensions.Y + "cm - " + imageInfo.PrintPaperType + "</font>";
            if (imageInfo.FrameCaption != string.Empty) fileSaleData = fileSaleData + "<br /><font size='1'>" + imageInfo.FrameCaption + "</font>";
            if (imageInfo.SecondCaption != string.Empty) fileSaleData = fileSaleData + "<br /></br ><font size='1'>" + imageInfo.SecondCaption + "</font>";
            
            //fileSaleData = fileSaleData + "<br /><font size='1'>Click <a href='http://www.windingart.com/mail/sendmail.aspx?image=" + encodedImageName + "'>here</a> to get more information<BR />All images � Dean Komen, all rights reserved</font>";

            return fileSaleData;
        }
        else
        {
            bool showSizeMessage = true;
            if (Contains(currentGaller, "Desktop"))
            {
                fileSaleData = "<font size='1'>";
                fileSaleData = fileSaleData + "Image is free. Right-Mouse click on image and select to save image";
                fileSaleData = fileSaleData + "</font>";
                //string encodedImageName = System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(fileName.Substring(6)));
            }
            else if (Contains(currentGaller, "Walker"))
            {
                fileSaleData = fileSaleData + "Dereck and Dana Walker";
            }
            else if (Contains(currentGaller, "Botha"))
            {
                fileSaleData = fileSaleData + "";
            }
            else if (Contains(currentGaller, "Shireen"))
            {
                fileSaleData = fileSaleData + "";
            }
            else if (!Contains(currentGaller, "Occasions") && !Contains(currentGaller, "Private"))
            {
                fileSaleData = "<font size='1'>";
                if (Contains(currentGaller, "MainPortfolio"))
                {
                    showSizeMessage = false;
                    fileSaleData = "Printed on Epson premium semi-gloss 250 or Hannemuhle Museum Etching 350<br />";
                    fileSaleData = fileSaleData + GetInfoMainPortfolio(fileName);
                }
                else{
                    fileSaleData = "";// "Prices range from R500-00 to R1250-00 depending on<br />the image in question, print size, frame and paper required.<br />";
                }
#region Old
                //else if (Contains(currentGaller, "Nature"))
                //{
                //    fileSaleData = GetInfoNature(fileName);
                //    if (fileSaleData.Trim() != string.Empty)
                //    {
                //        showSizeMessage = false;
                //        fileSaleData = "Printed on Epson premium semi-gloss 250<br />" + fileSaleData;
                //    }
                //    else
                //    {
                //        fileSaleData = "Printed on Epson premium semi-gloss 250GSM<br />"
                //        + "Prices range from R500-00 to R1250-00 depending on<br />"
                //        + "the image in question and the print size required.<br />";
                //    }
                //}
                //else if ((Contains(currentGaller, "Art") && !Contains(currentGaller, "WildlifeFineArt")))
                //{
                //    fileSaleData = GetInfoArtistic(fileName);
                //    if (fileSaleData.Trim() != string.Empty)
                //    {
                //        showSizeMessage = false;
                //        fileSaleData = "Printed on Epson premium semi-gloss 250<br />" + fileSaleData;
                //    }
                //    else
                //    {
                //        fileSaleData = "Printed on Epson premium semi-gloss 250GSM<br />"
                //        + "Prices range from R500-00 to R1250-00 depending on<br />"
                //        + "the image in question and the print size required.<br />";
                //    }
                //}
                //else if (Contains(currentGaller, "N1"))
                //{
                //    fileSaleData = fileSaleData + "Printed image price ZAR 300.00";
                //}
                //else if (Contains(currentGaller, "HumanBody"))
                //{
                //    showSizeMessage = false;
                //    fileSaleData = "Printed on Epson premium semi-gloss 250<br />";
                //    fileSaleData = fileSaleData + GetInfoHumanBody(fileName);
                //}
                //else if (Contains(currentGaller, "MainPortfolio") || Contains(currentGaller, "Shireen"))
                //{

                //}
                //else
                //{
                //    fileSaleData = fileSaleData + "Digital image price ZAR 100.00";
                //}
                //if (Contains(currentGaller, "MainPortfolio"))
                //{
                //    showSizeMessage = false;
                //    fileSaleData = "Printed on Epson premium semi-gloss 250 or Hannemuhle Museum Etching 350<br />";
                //    fileSaleData = fileSaleData + GetInfoMainPortfolio(fileName);
                //}
                //else
                //{
                //    if (showSizeMessage)
                //    {
                //        fileSaleData = fileSaleData + "<br />Image sizes range between 40cm and 80cm or more on the longest side";
                //    }
                //}
#endregion
                fileSaleData = fileSaleData + "</font>";

                //fileSaleData = fileSaleData + "<BR /><font size='1'>Click <a href='http://www.windingart.com/mail/sendmail.aspx?image=" + encodedImageName + "'>here</a> to get more info<BR />All images � Dean Komen, all rights reserved<br/></font>";
            }

            if (textToReturn != string.Empty)
            {
                textToReturn = textToReturn + "<BR/>";
            }

            textToReturn = textToReturn + fileSaleData;// GetFileSaleData(fileName);

            return textToReturn;
        }
    }

    private bool Contains(string fileName, string fileData)
    {
        return fileName.ToUpper().Contains(fileData.ToUpper());
    }

    private string GetInfoMainPortfolio(string currentFileName)
    {
        string info = string.Empty;
        int width = 0;
        int height = 0;

        if (Contains(currentFileName, "_MG_5453-BW"))
        {
            width = 5007;
            height = 2698;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleB, 1);
        }
        else if (Contains(currentFileName, "_MG_0021-Edit2"))
        {
            width = 4040;
            height = 2872;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA,1);
        }
        else if (Contains(currentFileName, "_MG_5443"))
        {
            width = 4535;
            height = 2632;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleB, 1);
        }
        else if (Contains(currentFileName, "_MG_0409-Edit-Edit"))
        {
            width = 5267;
            height = 3561;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "_MG_3502-Edit"))
        {
            width = 2879;
            height = 3383;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "_MG_5138-Edit"))
        {
            width = 3676;
            height = 2517;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "_MG_5144-Edit"))
        {
            width = 3882;
            height = 2761;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "_MG_5232-Edit"))
        {
            width = 3787;
            height = 2713;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "_MG_5261-Edit"))
        {
            width = 4063;
            height = 2897;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "_MG_5264-Edit"))
        {
            width = 4039;
            height = 2874;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "_MG_5268-Edit"))
        {
            width = 3874;
            height = 2737;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "_MG_5273-Edit"))
        {
            width = 2981;
            height = 3873;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "_MG_5345-BW") || Contains(currentFileName, "_MG_5351-BW") || Contains(currentFileName, "_MG_5381-BW"))
        {
            width = 3948;
            height = 2762;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleB, 1);
        }
        else if (Contains(currentFileName, "_MG_5359-Edit"))
        {
            width = 2782;
            height = 4157;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "_MG_5983-Edit"))
        {
            width = 2722;
            height = 3891;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "_MG_8342-Edit"))
        {
            width = 3882;
            height = 2702;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "FarmSunrise"))
        {
            width = 5439;
            height = 2592;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "IMG_2246-Edit"))
        {
            width = 3829;
            height = 2378;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "IMG_2265-Edit"))
        {
            width = 3836;
            height = 2684;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "IMG_2296-Edit"))
        {
            width = 3787;
            height = 2713;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleB, 1);
        }
        else if (Contains(currentFileName, "IMG_2333-Edit"))
        {
            width = 3788;
            height = 2713;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleB, 1);
        }
        else if (Contains(currentFileName, "IMG_2372-Edit2"))
        {
            width = 2619;
            height = 3881;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "IMG_2515-Edit"))
        {
            width = 3882;
            height = 2341;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        else if (Contains(currentFileName, "IMG_2719-Edit"))
        {
            width = 3733;
            height = 2468;
            info = PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, _scaleFactor1);
        }
        else if (Contains(currentFileName, "P1000433-Edit"))
        {
            width = 3590;
            height = 2289;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleB, 1);
        }
        else if (Contains(currentFileName, "_MG_0435-Edit-Edit"))
        {
            width = 5361;
            height = 3858;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }
        
        return info;
    }

    private string GetInfoNature(string currentFileName)
    {
        
        string info = string.Empty;
        int width = 0;
        int height = 0;

        if (Contains(currentFileName, "_MG_0435-Edit-Edit"))
        {
            width = 5361;
            height = 3858;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
        }

        return info;
    }

    private string GetInfoWildlifeFineArt(string currentFileName)
    {

        string info = string.Empty;
        int width = 0;
        int height = 0;

        if (Contains(currentFileName, "_MG_7336-Edit-Edit"))
        {
            width = 7763;
            height = 4040;
        }
        else if (Contains(currentFileName, "IMG_6102-Edit"))
        {
            width = 3449;
            height = 2675;
        }
        else if (Contains(currentFileName, "_MG_3043-Edit"))
        {
            width = 4047;
            height = 2948;
        }
        else if (Contains(currentFileName, "_MG_3142-Edit-Edit"))
        {
            width = 5589;
            height = 3144;
        }
        else if (Contains(currentFileName, "_MG_3142-Edit"))
        {
            width = 3847;
            height = 2760;
        }
        else if (Contains(currentFileName, "_MG_3144-Edit"))
        {
            width = 2714;
            height = 3791;
        }
        else if (Contains(currentFileName, "_MG_3165-Edit"))
        {
            width = 2838;
            height = 2009;
        }
        else if (Contains(currentFileName, "_MG_3190-Edit"))
        {
            width = 3976;
            height = 2808;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, _scaleFactor1);
        }
        else if (Contains(currentFileName, "_MG_7280-Edit"))
        {
            width = 3882;
            height = 2614;
        }
        else if (Contains(currentFileName, "_MG_7379-Edit-2"))
        {
            width = 4260;
            height = 3092;
        }
        else if (Contains(currentFileName, "_MG_7385-Edit"))
        {
            width = 3858;
            height = 2899;
        }
        else if (Contains(currentFileName, "_MG_7396-Edit"))
        {
            width = 4260;
            height = 3092;
        }
        else if (Contains(currentFileName, "_MG_7490-Edit"))
        {
            width = 4269;
            height = 3092;
        }
        else if (Contains(currentFileName, "_MG_7492-Edit"))
        {
            width = 3822;
            height = 2709;
        }
        else if (Contains(currentFileName, "_MG_7497-Edit"))
        {
            width = 4260;
            height = 3092;
        }
        else if (Contains(currentFileName, "Meerkat"))
        {
            width = 2173;
            height = 2905;
        }
        if (info == string.Empty)
        {
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, _scaleFactor1);
        }
        return info;
    }

    private string GetInfoHumanBody(string currentFileName)
    {
        string info = string.Empty;
        int width = 0;
        int height = 0;

        if (Contains(currentFileName, "_MG_9927-Edit"))
        {
            width = 2809;
            height = 3874;
            info = PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1.15) + "&nbsp;&nbsp;&nbsp;&nbsp;"
            + PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1 * 1.45) + "&nbsp;&nbsp;&nbsp;&nbsp;"
            + PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1 * 1.75);
        }
        else if (Contains(currentFileName, "_MG_0658-Edit2"))
        {
            width = 2523;
            height = 3364;
            info = PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1.15) + "&nbsp;&nbsp;&nbsp;&nbsp;"
            + PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1 * 1.45) + "&nbsp;&nbsp;&nbsp;&nbsp;"
            + PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1 * 1.75);
        }
        else if (Contains(currentFileName, "_MG_9938-Edit"))
        {
            width = 2688;
            height = 3795;
            info = PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1.15) + "&nbsp;&nbsp;&nbsp;&nbsp;"
            + PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1 * 1.45) + "&nbsp;&nbsp;&nbsp;&nbsp;"
            + PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1 * 1.75);
        }
        else if (Contains(currentFileName, "_MG_9943-Edit"))
        {
            width = 3976;
            height = 2808;
            info = PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1.15) + "&nbsp;&nbsp;&nbsp;&nbsp;"
            + PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1 * 1.45) + "&nbsp;&nbsp;&nbsp;&nbsp;"
            + PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1 * 1.75);
        }
        else if (Contains(currentFileName, "_MG_9962-Edit"))
        {
            width = 2808;
            height = 3976;
            info = PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1.15) + "&nbsp;&nbsp;&nbsp;&nbsp;"
            + PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1 * 1.45) + "&nbsp;&nbsp;&nbsp;&nbsp;"
            + PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1 * 1.75);
        }
        else if (Contains(currentFileName, "_MG_9945-Edit"))
        {
            width = 2400;
            height = 3278;
            info = PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1.15) + "&nbsp;&nbsp;&nbsp;&nbsp;"
            + PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1*1.55) + "&nbsp;&nbsp;&nbsp;&nbsp;"
            + PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1*2.15);
        }
        else if (Contains(currentFileName, "Melissa2-1-Edit"))
        {
            width = 1797;
            height = 2349;
            info = PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1.15) + "&nbsp;&nbsp;&nbsp;&nbsp;"
            + PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1*1.45) + "&nbsp;&nbsp;&nbsp;&nbsp;"
            + PrintDataLineFromPixelsAt240DPI(width, height, _standardPrintedImageCostScaleA, 1*1.80);
        }
        
        return info;
    }

    private string GetInfoArtistic(string currentFileName)
    {
        string info = string.Empty;
        int width = 0;
        int height = 0;

        if (Contains(currentFileName, "_MG_4414-Edit"))
        {
            width = 3843;
            height =2717;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleB, 1);
        }
        else if (Contains(currentFileName, "_MG_0885-Edit"))
        {
            width = 2336;
            height = 3504;
        }
        else if (Contains(currentFileName, "_MG_0906-Edit"))
        {
            width = 3504;
            height = 1929;
        }
        else if (Contains(currentFileName, "_MG_4417-Edit"))
        {
            width = 2674;
            height = 3841;    
        }
        else if (Contains(currentFileName, "_MG_4423-Edit"))
        {
            width = 3882;
            height = 2761;
            info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleB, 1);
        }
        else if (Contains(currentFileName, "_MG_1090-Edit"))
        {
            width = 3504;
            height = 2336;
        }
        else if (Contains(currentFileName, "IMGA0308"))
        {
            width = 3969;
            height = 3109;
        }
        else if (Contains(currentFileName, "VoicelessChild"))
        {
            width = 2646;
            height = 2671;
        }
        else if (Contains(currentFileName, "afort"))
        {
            width = 2362;
            height = 2406;
        }
        else if (Contains(currentFileName, "_MG_3313-Edit"))
        {
            width = 3487;
            height = 2308;
        }
        else if (Contains(currentFileName, "_MG_3322-Edit"))
        {
            width = 3504;
            height = 2559;
        }
        else if (Contains(currentFileName, "_MG_6652-Edit"))
        {
            width = 3822;
            height = 2614;
        }
        else if (Contains(currentFileName, "_MG_7428-Edit"))
        {
            width = 3882;
            height = 2761;
        }
        else if (Contains(currentFileName, ""))
        {
            width = 0;
            height = 0;
        }
        else if (Contains(currentFileName, ""))
        {
            width = 0;
            height = 0;
        }
        else if (Contains(currentFileName, ""))
        {
            width = 0;
            height = 0;
        }
        else if (Contains(currentFileName, ""))
        {
            width = 0;
            height = 0;
        }

        if (width != 0)
        {
            if (info == string.Empty)
            {
                info = PrintDataLineFromPixelsAt240DPIThreePrices(width, height, _standardPrintedImageCostScaleA, 1);
            }
        }
        return info;
    }

    private string PrintDataLineFromPixelsAt240DPIThreePrices(int widthPixels, int heightPixels, double randPrice, double scalingFactor)
    {
        double widthCentimeters = (widthPixels / 240) * 2.54;
        double heightCentimeters = (heightPixels / 240) * 2.54;
        return PrintDataLineFromCentimeters(widthCentimeters, heightCentimeters, randPrice, scalingFactor)
            + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + PrintDataLineFromCentimeters(widthCentimeters, heightCentimeters, randPrice, scalingFactor * _scaleFactor1)
            + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + PrintDataLineFromCentimeters(widthCentimeters, heightCentimeters, randPrice, scalingFactor * _scaleFactor2);
    }

    private string PrintDataLineFromPixelsAt240DPI(int widthPixels, int heightPixels, double randPrice, double scalingFactor)
    {
        double widthCentimeters = (widthPixels / 240) * 2.54;
        double heightCentimeters = (heightPixels / 240) * 2.54;
        return PrintDataLineFromCentimeters(widthCentimeters, heightCentimeters, randPrice, scalingFactor);
    }

    private string PrintDataLineFromCentimeters(double widthCentimeters, double heightCentimeters, double randPrice, double scalingFactor)
    {
        double paperCost = GetPaperCostFromSize(widthCentimeters * scalingFactor, heightCentimeters * scalingFactor);
        double width = (int)((widthCentimeters * scalingFactor));
        double height = (int)((heightCentimeters * scalingFactor));
        double price = (int)(randPrice * scalingFactor);
        price = RoundOffPrice(price);
        if (paperCost == _noPrice)
        {
            return width + "cm x " + height + "cm (price on request)";
        }
        else
        {
            return width + "cm x " + height + "cm R" + (price + paperCost) + ".00";
        }
    }

    private double RoundOffPrice(double price)
    {
        double workingPrice = price;
        double integralAmount = 0;
        double decimalAmount = 0;
        if ((price >= 100) && (price <= 9999.99))
        {
            workingPrice = price / 100;

            integralAmount = System.Math.Floor(workingPrice);
            decimalAmount = workingPrice - integralAmount;

            if ((decimalAmount > 0) && (decimalAmount<= 0.25))
            {
                decimalAmount = 0.25;
            }
            else if ((decimalAmount > 0.25) && (decimalAmount<= 0.5))
            {
                decimalAmount = 0.5;
            }
            else if ((decimalAmount > 0.5)&&(decimalAmount<= 0.75))
            {
                decimalAmount = 0.75;
            }
            else
            {
                decimalAmount = 0;
                integralAmount++;
            }

            price = (integralAmount + decimalAmount) * 100;
        }
        
        return price;
        
    }
    
    /// <summary>
    /// Calculate the cost of the print paper based on the paper size.
    /// Paper type is 250GSM Epson Premium Semi-Gloss
    /// </summary>
    /// <param name="widthCentimeters"></param>
    /// <param name="heightCentimeters"></param>
    /// <returns></returns>
    double GetPaperCostFromSize(double widthCentimeters, double heightCentimeters)
    {
        PaperSize papersize = GetPaperSize(widthCentimeters, heightCentimeters);
        switch (papersize)
        {
            case PaperSize.A0:
                return 600.00;
            case PaperSize.A1:
                return 300.00;
            case PaperSize.A2:
                return 150.00;
            case PaperSize.A3:
                return 100.00;
            case PaperSize.A4:
                return 50.00;
            case PaperSize.A5:
                return 25.00;
            case PaperSize.A6:
                return 20.00;
            case PaperSize.Undefined:
                return _noPrice;
            default:
                return _noPrice;
        }
    }
    /// <summary>
    /// Get the closest printer paper size for the image
    /// </summary>
    /// <param name="widthCentimeters"></param>
    /// <param name="heightCentimeters"></param>
    /// <returns></returns>
    PaperSize GetPaperSize(double widthCentimeters, double heightCentimeters)
    {
        PaperSize paperSize = PaperSize.A0;

        double longestSideCentimeters = widthCentimeters >= heightCentimeters ? widthCentimeters : heightCentimeters;


        if ((longestSideCentimeters <= 14.8) && (longestSideCentimeters > 0)) //14.8
        {
            paperSize = PaperSize.A6;
        }
        else if ((longestSideCentimeters <= 21) && (longestSideCentimeters > 14.8))
        {
            paperSize = PaperSize.A5;
        }
        else if ((longestSideCentimeters <= 29.7) && (longestSideCentimeters > 21))
        {
            paperSize = PaperSize.A4;
        }
        else if ((longestSideCentimeters <=42 ) && (longestSideCentimeters > 29.7))
        {
            paperSize = PaperSize.A3;
        }
        else if ((longestSideCentimeters <= 59.4) && (longestSideCentimeters > 42))
        {
            paperSize = PaperSize.A2;
        }
        else if ((longestSideCentimeters <= 84.1) && (longestSideCentimeters > 59.4))
        {
            paperSize = PaperSize.A1;
        }
        else if ((longestSideCentimeters <=118.9))
        {
            paperSize = PaperSize.A0;
        }
        else
        {
            paperSize = PaperSize.Undefined;
        }
        
        return paperSize;
    }
}

