﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public class FileComparer : System.Collections.IComparer
{
    public enum CompareBy
    {
        Name /* a-z */,
        LastWriteTime /* oldest to newest */,
        CreationTime  /* oldest to newest */,
        LastAccessTime /* oldest to newest */,
        FileSize /* smallest first */
    }
    // default comparison
    int _CompareBy = (int)CompareBy.Name;

    public FileComparer()
    {
    }

    public FileComparer(CompareBy compareBy)
    {
        _CompareBy = (int)compareBy;
    }

    int System.Collections.IComparer.Compare(object x, object y)
    {
        int output = 0;
        System.IO.FileInfo file1 = new System.IO.FileInfo(x.ToString());
        System.IO.FileInfo file2 = new System.IO.FileInfo(y.ToString());
        switch (_CompareBy)
        {
            case (int)CompareBy.LastWriteTime:
                output = DateTime.Compare(file1.LastWriteTime, file2.LastWriteTime);
                break;
            case (int)CompareBy.CreationTime:
                output = DateTime.Compare(file1.CreationTime, file2.CreationTime);
                break;
            case (int)CompareBy.LastAccessTime:
                output = DateTime.Compare(file1.LastAccessTime, file2.LastAccessTime);
                break;
            case (int)CompareBy.FileSize:
                output = Convert.ToInt32(file1.Length - file2.Length);
                break;
            case (int)CompareBy.Name:
            default:
                output = (new System.Collections.CaseInsensitiveComparer()).Compare(file1.Name, file2.Name);
                break;
        }
        return output;
    }
}

