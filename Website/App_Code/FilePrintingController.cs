using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for FilePrintingController
/// </summary>
[Serializable]
public class FilePrintingController
{
    #region Fields
    private FilePrinting _filePrintingDataSet = new FilePrinting();
    #endregion
    #region Constructors
    public FilePrintingController()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion
    #region Public Methods
    public FilePrinting GetImageDataSet(string existingImageFileName)
    {
        //GetPrint papers
        (new FilePrintingTableAdapters.PrintPaperTableAdapter()).Fill(_filePrintingDataSet.PrintPaper, null);

        //Get the exisitng files data - if it exists
        if (existingImageFileName != string.Empty)
        {
            (new FilePrintingTableAdapters.ImagesTableAdapter()).Fill(_filePrintingDataSet.Images, existingImageFileName);
            (new FilePrintingTableAdapters.ImagePrintsTableAdapter()).Fill(_filePrintingDataSet.ImagePrints, existingImageFileName);
        }
        return _filePrintingDataSet;
    }
    public void SaveImageDataSet()
    {
        (new FilePrintingTableAdapters.ImagesTableAdapter()).Update(_filePrintingDataSet.Images);
        (new FilePrintingTableAdapters.ImagePrintsTableAdapter()).Update(_filePrintingDataSet.ImagePrints);
    }
    #endregion
    #region Properties
    public FilePrinting FilePrintingDataSet
    {
        get
        {
            return _filePrintingDataSet;
        }
        set
        {
            _filePrintingDataSet = value;
        }
    }
    #endregion
}
