using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for BasePage
/// </summary>
public class BasePage : System.Web.UI.Page
{
	public BasePage() 
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string GeneratePage(Page currentPage, Enums.SortOrder thumbnailSortOrder)
    {
        return GeneratePage(currentPage, thumbnailSortOrder,3,5);       
    }
    public string GeneratePage(Page currentPage, Enums.SortOrder thumbnailSortOrder, int rows, int columns)
    {
        PageFunctions.DoPageHitCounter(currentPage);

        int indexPageToLoad = 0;
        if (Request.QueryString["pageIndex"] != null)
        {
            indexPageToLoad = int.Parse(Request.QueryString["pageIndex"]);
        }

        GenerateSitePages pageGenerator = new GenerateSitePages(Server, rows, columns);
        System.Collections.Generic.List<PageTable> pageTables = pageGenerator.GetThumbNailsTables(Server.MapPath("thumbs"), thumbnailSortOrder);

        Page.ClientScript.RegisterStartupScript(this.GetType(), "jav", pageGenerator.GetClientSideScript());

        return pageTables[indexPageToLoad].ThumbnailsTableHTML + "<BR /><font color='grey'> Page " + (indexPageToLoad + 1) + " of " + pageTables.Count + "</font><BR />";
    }
    public string GeneratePage(Page currentPage)
    {
        return GeneratePage(currentPage, Enums.SortOrder.LIFO);        
    }
    
}
