﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Summary description for PageFunctions
/// </summary>
public static class PageFunctions
{
    public static void PageHitCounter(object data)
    {
        try
        {

	    ItemPair parameters = (ItemPair)data;
	    string page = "";
	    if (parameters.AdditionalData.ToLower() == "stumbleupon")
            {
                page = System.IO.Path.GetDirectoryName("(SU) " + parameters.HttpRequestData.FilePath).Replace("\\", "/") + "/";
            }
            else
            {
                page = System.IO.Path.GetDirectoryName(parameters.HttpRequestData.FilePath).Replace("\\", "/") + "/" + parameters.AdditionalData;
            }

	    derotek.Utilities.PageHitCounter.Counter.AddHit("WindingArt",DateTime.Now,page,parameters.HttpRequestData.UserHostAddress);
		
        }
        catch (Exception ex)
        {
            //throw ex;
        }
    }
    public static void PageHitCounter(HttpRequest request)
    {
        string additionalData = string.Empty;
        ItemPair itemPair = new ItemPair(request, additionalData);
        //PageHitCounter(itemPair);
        System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(PageHitCounter),itemPair);
    }
    public static void PageHitCounter(HttpRequest request, string additionalData)
    {
        ItemPair itemPair = new ItemPair(request, additionalData);
        //PageHitCounter(itemPair);
        System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(PageHitCounter),itemPair);
    }
    public class ItemPair
    {
        #region Fields
        HttpRequest _httpRequest = null;
        string _additionalData = string.Empty;
        #endregion
        #region Constructors
        public ItemPair(HttpRequest httpRequest, string additionalData)
        {
            _httpRequest = httpRequest;
            _additionalData = additionalData;
        }
        #endregion
        #region Properties
        public HttpRequest HttpRequestData
        {
            get
            {
                return _httpRequest;
            }
        }
        public string AdditionalData
        {
            get
            {
                return _additionalData;
            }
        }
        #endregion

    }
    public static void DoPageHitCounter(System.Web.UI.Page webPage)
    {
        if (webPage.Session["IgnorePageHits"] == null)
        {
            if (webPage.Session["stumbleupon"] != null)
            {
                PageFunctions.PageHitCounter(webPage.Request, "stumbleupon");
            }
            else
            {
                PageFunctions.PageHitCounter(webPage.Request);
            }
        }
    }
}
