using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Contains the HTML for a single table containing thumbnails
/// </summary>
public class ThumbnailsTable
{
    #region Fields
    private System.Web.HttpServerUtility _httpServer;
    private int _thumbnailsMaxRowsCount;                                                    //The maximum number of rows in the thumbnails table
    private int _thumbnailsMaxColumnsCount;                                                 //The maximum number of columns in the thumbnails table
    private string _highlightedThumbnailTableCellColor;                                     //The color that a thumbnail cell must change to when the mouse hovers over it
    private string _unselectedCellBackgroundColor;
    private string _thumbnailImageBorderColor;                                              //The border color of the actual thumbnail image in a table cell
    private int _thumbTableCellSideLength;                                                  //The length of the sides of each thumbnail cell in the table
    private System.Collections.Generic.List<string> _thumbnailImageFilesToAddToTable;       //A collection of path to the individual image thumbnails\files to show in the table
    #endregion
    #region Constructors
    public ThumbnailsTable(System.Web.HttpServerUtility httpServer, int thumbnailsMaxRowsCount, int thumbnailsMaxColumnsCount, string unselectedCellBackgroundColor, string highlightedThumbnailTableCellColor, string thumbnailImageBorderColor, int thumbTableCellSideLength, System.Collections.Generic.List<string> thumbnailImageFilesToAddToTable)
    {
        _httpServer = httpServer;
        _thumbnailsMaxRowsCount = thumbnailsMaxRowsCount;
        _thumbnailsMaxColumnsCount = thumbnailsMaxColumnsCount;
        _unselectedCellBackgroundColor = unselectedCellBackgroundColor;
        _highlightedThumbnailTableCellColor = highlightedThumbnailTableCellColor;
        _thumbnailImageBorderColor = thumbnailImageBorderColor;
        _thumbTableCellSideLength = thumbTableCellSideLength;
        _thumbnailImageFilesToAddToTable = thumbnailImageFilesToAddToTable;
    }
    #endregion
    #region Public Functions
    public string GetThumbNailsTable(int pageIndex, string httpPathToThumbnails, bool anotherPageToFollow)
    {
        int currentFileIndex = 0;
        string currentImageFileNamePath = string.Empty;
        if (!httpPathToThumbnails.EndsWith("/"))
        {
            httpPathToThumbnails = httpPathToThumbnails + "/";
        }
        int imageCounter = pageIndex * (_thumbnailsMaxColumnsCount * _thumbnailsMaxRowsCount) + 1;
        System.Text.StringBuilder thumbNailTable = new System.Text.StringBuilder();
        thumbNailTable.AppendLine("<table style='border-style:solid;border-width:0px;border-spacing:0px;padding:0px;color:gray;border-collapse:collapse;' >");
        for (int tableRowCounter = 1; tableRowCounter <= _thumbnailsMaxRowsCount; tableRowCounter++)
        {
            thumbNailTable.AppendLine("   <tr>");
            for (int tableColumnCounter = 1; tableColumnCounter <= _thumbnailsMaxColumnsCount; tableColumnCounter++)
            {
                thumbNailTable.AppendLine("        <td align='left' style='left;border-style:solid;border-width:1px;padding:0px;border-style:inset;cell-spacing:0px;cell-padding:0px;'>");
                //thumbNailTable.AppendLine("<div id='uiThumbnailImageCounter' style='position: relative;top:10px;left10px'>" + currentFileIndex.ToString() + "</div>");
                thumbNailTable.AppendLine("            <div id='id" + currentFileIndex.ToString() + "' style='background-color:" + _unselectedCellBackgroundColor + ";width:" + _thumbTableCellSideLength + "px;height:" + _thumbTableCellSideLength + "px' onmouseover=\"HoverOverTable('ON','id" + currentFileIndex.ToString() + "')\" onmouseout=\"HoverOverTable('OFF','id" + currentFileIndex.ToString() + "')\">");
                if ((currentFileIndex) <= _thumbnailImageFilesToAddToTable.Count - 1)
                {
                    currentImageFileNamePath = _thumbnailImageFilesToAddToTable[currentFileIndex];
                    string imagePath = httpPathToThumbnails + System.IO.Path.GetFileName(currentImageFileNamePath);
                    System.Drawing.Size size = GeneralFunctions.GetImageSize(currentImageFileNamePath);
                    int top = GeneralFunctions.CalculateForCentre(_thumbTableCellSideLength, size.Height);
                    int left = GeneralFunctions.CalculateForCentre(_thumbTableCellSideLength, size.Width);
                    string prevFileData = GetEncodedFilePathForLargeImage(_thumbnailImageFilesToAddToTable, currentFileIndex - 1);
                    string currentFileData = GetEncodedFilePathForLargeImage(_thumbnailImageFilesToAddToTable, currentFileIndex);
                    string nextFileData = GetEncodedFilePathForLargeImage(_thumbnailImageFilesToAddToTable, currentFileIndex + 1);
                    string fileDataParams = "prev=" + prevFileData + "&current=" + currentFileData + "&next=" + nextFileData;
                    thumbNailTable.AppendLine("<div id='uiThumbnailImageCounter' style='font-size:16pt;color:#3a3a3a;font-weight:bold;position: relative;top:5px;left:6px'>" + imageCounter.ToString() + "</div>");
                    imageCounter++;
                    thumbNailTable.AppendLine("            <div style='position:relative;top:" + (top - 30).ToString() + "px;left:" + left.ToString() + "px'>");

                    thumbNailTable.AppendLine("                <a href='large/" + System.IO.Path.GetFileName(_thumbnailImageFilesToAddToTable[currentFileIndex]) + "' >");
                    //thumbNailTable.AppendLine("                    <img src='" + imagePath + "' style='border:" + _thumbnailImageBorderColor + " thin solid;' height='0' width='0' />");
                    //thumbNailTable.AppendLine("                </a>");

                    thumbNailTable.AppendLine("                  <a href='large.aspx?" + fileDataParams + "' >");
                    thumbNailTable.AppendLine("                    <img src='" + imagePath + "' style='border:" + _thumbnailImageBorderColor + " thin solid;' />");
                    thumbNailTable.AppendLine("                  </a>");
                    thumbNailTable.AppendLine("                </a>");
                    thumbNailTable.AppendLine("            </div>");
                }
                thumbNailTable.AppendLine("            </div>");
                thumbNailTable.AppendLine("        </td>");
                currentFileIndex++;
            }
            thumbNailTable.AppendLine("   </tr>");
        }
        thumbNailTable.AppendLine("</table>");

        thumbNailTable.AppendLine("<table>");
        thumbNailTable.AppendLine(" <tr>");
        thumbNailTable.AppendLine("     <td align='right'>");
        thumbNailTable.AppendLine("         <div style='width:" + ((_thumbTableCellSideLength * _thumbnailsMaxColumnsCount) - 35).ToString() + "px'>");
        thumbNailTable.AppendLine("             <b>");



        if (pageIndex > 0)
        {
            thumbNailTable.AppendLine("<a href='default.aspx?pageIndex=" + (pageIndex - 1).ToString() + "' color='#626262' size='smaller'>Prev</a>");
        }
        else
        {
            thumbNailTable.AppendLine("<font color=#666666>Prev</font>");
        }
        thumbNailTable.AppendLine("&nbsp;");
        if (anotherPageToFollow)
        {
            thumbNailTable.AppendLine("<a href='default.aspx?pageIndex=" + (pageIndex + 1).ToString() + "' color='#626262'>Next</a>");
        }
        else
        {
            thumbNailTable.AppendLine("<font color=#666666>Next</font>");
        }


        thumbNailTable.AppendLine("             </b>");
        thumbNailTable.AppendLine("         </div>");
        thumbNailTable.AppendLine("     </td>");
        thumbNailTable.AppendLine(" </tr>");

        return thumbNailTable.ToString();
    }
    #endregion
    #region Private Functions
    /// <summary>
    /// Searches a list of files for the file path with the index requested.
    /// If the file index is valid then change the path to point to the large images and not the thumb nails.
    /// </summary>
    /// <param name="files"></param>
    /// <param name="indexOfFileToRetreive"></param>
    /// <returns></returns>
    private string GetEncodedFilePathForLargeImage(System.Collections.Generic.List<string> files, int indexOfFileToRetreive)
    {
        if ((indexOfFileToRetreive < 0) || (indexOfFileToRetreive > files.Count - 1))
        {
            return string.Empty;
        }
        else
        {
            //return System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(files[indexOfFileToRetreive].ToLower().Replace("/thumbs/", "/large/")));
            string fileName = files[indexOfFileToRetreive].ToLower();
            string filePath = "large/" + System.IO.Path.GetFileName(fileName);
            return System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(filePath));
        }
    }
    #endregion
}
