using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for GenerateSitePages
/// </summary>
public class GenerateSitePages
{
    #region Constants
    const string _highlightedThumbnailTableCellColor = "#808080";                           //The color that a thumbnail cell must change to when the mouse hovers over it
    const string _thumbnailImageBorderColor = "#252525";                                    //The border color of the actual thumbnail image in a table cell
    const string _unselectedCellBackgroundColor = "#333333";
    const int _thumbTableCellSideLength = 160;                                              //The length of the sides of each thumbnail cell in the table
    #endregion
    #region Fields
    private System.Web.HttpServerUtility _httpServer;
    private int _thumbnailsMaxRowsCount;                                                    //The maximum number of rows in the thumbnails table
    private int _thumbnailsMaxColumnsCount;                                                 //The maximum number of columns in the thumbnails table
    #endregion
    #region Constructors
    public GenerateSitePages(System.Web.HttpServerUtility server, int thumbnailsMaxRowsCount, int thumbnailsMaxColumnsCount)
    {
        _httpServer = server;
        _thumbnailsMaxRowsCount = thumbnailsMaxRowsCount;
        _thumbnailsMaxColumnsCount = thumbnailsMaxColumnsCount;
    }
    #endregion
    #region Public Functions
    public string GetClientSideScript()
    {
        string highlightingThumbnailTableCell = "" +
        "   function HoverOverTable(visibility, id) " +
        "   {" +
        "       var control = document.getElementById(id)" + System.Environment.NewLine +
        "       if(visibility=='ON')" + System.Environment.NewLine +
        "       {" + System.Environment.NewLine +
        "           control.style.backgroundColor='" + _highlightedThumbnailTableCellColor + "';" + System.Environment.NewLine +
        "       }" + System.Environment.NewLine +
        "       else" + System.Environment.NewLine +
        "       {" + System.Environment.NewLine +
        "           control.style.backgroundColor='" + _unselectedCellBackgroundColor + "';" + System.Environment.NewLine +
        "       }" + System.Environment.NewLine +
        "   }";

        return "<script language='javascript' type='text/javascript'>" + highlightingThumbnailTableCell + "</script>";
    }
    public System.Collections.Generic.List<PageTable> GetThumbNailsTables(string pathToThumbnails, Enums.SortOrder thumbNailsSortOrder)
    {
        System.Collections.Generic.List<PageTable> pageTables = new System.Collections.Generic.List<PageTable>();
        string[] thumbnailFiles = System.IO.Directory.GetFiles(pathToThumbnails,"*.jpg");
        System.Collections.IComparer comp;
        switch(thumbNailsSortOrder)
        {
            case Enums.SortOrder.LIFO:
                comp = new FileComparer(FileComparer.CompareBy.CreationTime);
                Array.Sort(thumbnailFiles, comp);
                Array.Reverse(thumbnailFiles);
                break;
            case Enums.SortOrder.FIFO:
                comp = new FileComparer(FileComparer.CompareBy.CreationTime);
                Array.Sort(thumbnailFiles, comp);
                break;
            case Enums.SortOrder.Alphanumeric:
                comp = new FileComparer(FileComparer.CompareBy.Name);
                Array.Sort(thumbnailFiles, comp);
                break;
            case Enums.SortOrder.ReverseAlphanumeric:
                comp = new FileComparer(FileComparer.CompareBy.Name);
                Array.Sort(thumbnailFiles, comp);
                Array.Reverse(thumbnailFiles);
                break;
            case Enums.SortOrder.FromXmlFile:
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(pathToThumbnails + "/DisplayOrder.xml");
                System.Xml.XmlNodeList list = doc.SelectNodes("displayorder/files/file");
                thumbnailFiles = new string[list.Count];
                for (int currentFile=0;currentFile<list.Count;currentFile++)
                {
                   thumbnailFiles[currentFile] = System.IO.Path.Combine(pathToThumbnails, list[currentFile].Attributes["name"].Value);
                }

                break;
        }
        
        System.Collections.Generic.Dictionary<string, string> thumbnailTableForFile = new System.Collections.Generic.Dictionary<string, string>();
        int currentFileIndex = 0;
        int fileCounterForCurrentGeneratedTable = 0;
        int pageCounter = 0;
        for (int file = 0; file <= thumbnailFiles.Length - 1; file += (_thumbnailsMaxRowsCount * _thumbnailsMaxColumnsCount))
        {
            fileCounterForCurrentGeneratedTable = 0;
            System.Collections.Generic.List<string> imagesToAddToTable = new System.Collections.Generic.List<string>();

            while ((currentFileIndex < thumbnailFiles.Length))
            {
                imagesToAddToTable.Add(thumbnailFiles[currentFileIndex]);
                currentFileIndex++;
                fileCounterForCurrentGeneratedTable++;
                if (fileCounterForCurrentGeneratedTable == (_thumbnailsMaxRowsCount * _thumbnailsMaxColumnsCount))
                {
                    break;
                }
            }
            bool anotherPageToFollow = ((file + (_thumbnailsMaxRowsCount * _thumbnailsMaxColumnsCount)) < thumbnailFiles.Length);
            ThumbnailsTable thumbnailsTable = new ThumbnailsTable(_httpServer, _thumbnailsMaxRowsCount, _thumbnailsMaxColumnsCount, _unselectedCellBackgroundColor, _highlightedThumbnailTableCellColor, _thumbnailImageBorderColor, _thumbTableCellSideLength, imagesToAddToTable);
            string tableData = thumbnailsTable.GetThumbNailsTable(pageCounter, "Thumbs", anotherPageToFollow);
            if (pageCounter == 0)
            {
                pageTables.Add(new PageTable("default.aspx", tableData));
            }
            else
            {
                pageTables.Add(new PageTable("default" + pageCounter.ToString() + ".aspx", tableData));
            }
            pageCounter++;
        }
        return pageTables;
    }
    #endregion
}
