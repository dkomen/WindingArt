using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Path.ToUpper().Contains("FRANCO21"))
        {
            mediaRss.Text = "<link rel='alternate' href='http://www.windingart.com/Private/Franco21/photos.rss' type='application/rss+xml' title='' id='gallery' />";
        }
        else if (Request.Path.ToUpper().Contains("OCCASSIONS"))
        {
            mediaRss.Text = "<link rel='alternate' href='http://www.windingart.com/Pages/Occassions/photos.rss' type='application/rss+xml' title='' id='gallery' />";
        }
        else if (Request.Path.ToUpper().Contains("NATURE"))
        {
            mediaRss.Text = "<link rel='alternate' href='http://www.windingart.com/Pages/Nature/photos.rss' type='application/rss+xml' title='' id='gallery' />";
        }
        else if (Request.Path.ToUpper().Contains("BOTANICAL_GARDENS"))
        {
            mediaRss.Text = "<link rel='alternate' href='http://www.windingart.com/Pages/BOTANICAL_GARDENS/photos.rss' type='application/rss+xml' title='' id='gallery' />";
        }
        else if (Request.Path.ToUpper().Contains("SJ21"))
        {
            mediaRss.Text = "<link rel='alternate' href='http://www.windingart.com/Private/SJ21/photos.rss' type='application/rss+xml' title='' id='gallery' />";
        }
        else if (Request.Path.ToUpper().Contains("SWARTKOPS"))
        {
            mediaRss.Text = "<link rel='alternate' href='http://www.windingart.com/Pages/Swartkops/photos.rss' type='application/rss+xml' title='' id='gallery' />";
        }
        else if (Request.Path.ToUpper().Contains("SODWANA2008"))
        {
            mediaRss.Text = "<link rel='alternate' href='http://www.windingart.com/Private/Sodwana2008/photos.rss' type='application/rss+xml' title='' id='gallery' />";
        }
        else
        {
            mediaRss.Text = "<link rel='alternate' href='http://www.windingart.com/Pages/Art/photos.rss' type='application/rss+xml' title='' id='gallery' />";
        }

        string galleryName = Server.MapPath("");
        galleryName = System.IO.Path.GetFileNameWithoutExtension(galleryName);
        HyperLink2.Visible = !(System.IO.Path.GetFileName(Server.MapPath("")).ToLower().Contains("photo"));
        if (HyperLink2.Visible)
        {
            this.uxGalleryName.Text = galleryName.Replace('_',' ') + "&nbsp;";
        }
        else
        {
            this.uxGalleryName.Text = string.Empty;
        }


        uxLinkPrints.Visible = !(galleryName.ToLower().Contains("prints"));
        if (!uxLinkPrints.Visible)
        {
            uxPricingLink.Visible = false;
            uxLinkPrintsSeperator.Visible = uxPricingLink.Visible;
            HyperLink3.Visible = uxPricingLink.Visible;
            Label1.Visible = uxPricingLink.Visible;
            Label3.Visible = uxPricingLink.Visible;
        }
    }
}
