﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageDefault.master" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <BR/>
    <BR/>
    <BR/>
    <BR/>
    <BR/>
    You can contact me regarding purchases of packages and art works at <a href="mailto:photo@windingart.com">photo@windingart.com</a><br />
    Feal free to discuss framing, alternate print sizes and delivery options for your orders.
    <br />
    <br />
    Prints are done with longevity in mind and as such age resistant Hahnemuhle papers are used for fine-art works and<br />
    are typically printed on using the professional Epson Stylus Pro 9900 with an HDR pigment based ink system.<br />
    <br />
    Prints can also be sprayed with the Hahnemuhle Protective Spray for extra protection.<br />
    As of September 2009 Hahnemuhle print certificates are also available for many of the art works.
    <br />
    If properly looked after prints could easily last fifty years or even upto to a century before any noticeable degradation.<br />
    <br />
    <br />
    <br />
    Galleries where my works have been dislayed :     
    <br />
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Lisa Wendt Gallery, Honeydew, Johannesburg<br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Apple Framing and Art Gallery, Garsfontein, Pretoria<br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/_MG_0064.jpg" 
        style="position:absolute; top: 442px; left: 99px;"/>
    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/_MG_0047.jpg" 
        style="position:absolute; top: 326px; left: 479px; height: 217px; width: 352px;"/>
    
</asp:Content>

