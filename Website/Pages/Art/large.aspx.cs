﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class Pages_Art_large : BaseLargeImagePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string data = GeneratePage(Page);
        if (data.Length <= 210)
        {
            Response.Redirect("large/zoomify.aspx?current=" + Page.Request.QueryString["current"]);
        }
        else
        {
            uxHtmlLiteral.Text = data;
        }
    }
}
