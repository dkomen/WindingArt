﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class Pages_Artistic_Default : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        uiThumbNailTable.Text = GeneratePage(Page);
    }
}
