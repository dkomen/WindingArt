using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterPageDefault : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetIconAttributes(MenuGalleries, "Galleries.aspx");
        //SetIconAttributes(NewMenuSlideshow, "Default1.aspx?speed=2");
        SetIconAttributes(MenuWeddings, "http://testplatform.ddns.net/wedding");
        SetIconAttributes(MenuAbout, "about.aspx");
        //SetIconAttributes(MenuCourses, "courses.aspx");
    }

    private void SetIconAttributes(Image imageIcon, string navigationUrl)
    {
        imageIcon.Attributes.Add("onmouseover", "SetImage('" + imageIcon.ClientID + "', 'Images/" + imageIcon.ID + ".png')");
        imageIcon.Attributes.Add("onmouseout", "SetImage('" + imageIcon.ClientID + "', 'Images/" + imageIcon.ID + "Out.png')");
        imageIcon.Attributes.Add("onclick", "NavigateTo('" + navigationUrl + "')");
    }
}
