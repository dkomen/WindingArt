using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["ph"] == "0")
        {
            Session.Add("stumbleupon", "1");
            Session.Remove("IgnorePageHits");
        }
        else if (Request.QueryString["ph"] == "1")
        {
            Session.Add("IgnorePageHits", "1");
            Session.Remove("stumbleupon");
        }

        
        //SetIconAttributes(MirrorOccasions, "Pages/Occasions/default.aspx", "special occasions");
        SetIconAttributes(MirrorMainPortfolio, "Pages/MainPortfolio/default.aspx", "main portfolio");
        //SetIconAttributes(MirrorLandscape, "Pages/Landscapes/default.aspx", "landscapes");
        SetIconAttributes(MirrorNature, "Pages/Nature/default.aspx", "the natural world");
        SetIconAttributes(MirrorArt, "Pages/Art/default.aspx", "artistic");

        //SetIconAttributesSubMenu(ImageGenerator, "CoverDesigner/default.aspx");
        //SetIconAttributesSubMenu(Desktop, "Pages/DesktopBackgrounds/default.aspx");
        //SetIconAttributesSubMenu(Botanical_Gardens, "Pages/Botanical_Gardens/default.aspx");
        SetIconAttributesSubMenu(N1, "Pages/N1/default.aspx");
        //SetIconAttributesSubMenu(Swartkops, "Pages/Swartkops/default.aspx");
        //SetIconAttributesSubMenu(Sodwana, "Pages/Sodwana/default.aspx");
        SetIconAttributesSubMenu(HumanBody, "Pages/HumanBody/default.aspx");

        if (Session["IgnorePageHits"] == null)
        {
            if (Session["stumbleupon"] != null)
            {
                PageFunctions.PageHitCounter(Request, "stumbleupon");
            }
            else
            {
                PageFunctions.PageHitCounter(Request);
            }
        }
    }

    private void SetIconAttributes(Image imageIcon, string navigationUrl, string category)
    {
        imageIcon.Attributes.Add("onmouseover", "SetImage('" + imageIcon.ClientID + "', 'Images/" + imageIcon.ID + ".jpg', '" + category + "', '" + uiCatagory.ClientID + "')");
        imageIcon.Attributes.Add("onmouseout", "SetImage('" + imageIcon.ClientID + "', 'Images/" + imageIcon.ID + "Out.jpg', '', '" + uiCatagory.ClientID + "')");
        imageIcon.Attributes.Add("onclick", "NavigateTo('" + navigationUrl + "')");
    }
    private void SetIconAttributesSubMenu(Image imageIcon, string navigationUrl)
    {
        imageIcon.Attributes.Add("onmouseover", "SetImage('" + imageIcon.ClientID + "', 'Images/SubMenu/" + imageIcon.ID + ".jpg')");
        imageIcon.Attributes.Add("onmouseout", "SetImage('" + imageIcon.ClientID + "', 'Images/SubMenu/" + imageIcon.ID + "Out.jpg')");
        imageIcon.Attributes.Add("onclick", "NavigateTo('" + navigationUrl + "')");
    }
}
