﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageDefault.master" AutoEventWireup="true" CodeFile="Courses.aspx.cs" Inherits="Courses" %>
<asp:content id="Header1" ContentPlaceHolderID="HeaderPlaceHolder1" runat="server">
    <style>
        h1
        {
            font-size:12pt;
            color:mediumaquamarine;
            text-decoration:underline;
        }
        h2
        {
            font-size:14pt;
            color:aqua;
            text-decoration:underline;
        }
        .mod
        {
            font-size:9pt;
            color:rgb(90,90,90);
        }
        article
        {
            padding-left:20px;
        }
    </style>        
</asp:content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <br />
    <br />
    <br />
    <br />
    <h2>Courses and their fees</h2>
    <article>
        For online courses any custom selection of course modules may be taken.<br />
        If private face-to-face tuition (presented at the Pretoria Botanical Gardens in Pretoria-East) is required then the full course fee will be payable 
        and all modules for the course selected will be presented.<br />
        Courses are indicated with the prefix CRS and modules with the prefix MOD.
    </article>

    <h1>CRS101: Lets go a step further than typical snapshots(1 hour: R350-00)</h1>
    <article>
        <span class="mod">MOD101-1:</span> What exposure is and how to control it with shutter speed, lense aperture and sensor sensitivity <br />
        <span class="mod">MOD101-2:</span> How to compose your photos and introducing the rule-of-thirds<br />
    </article>
    <h1>CRS102: The D-SLR camera photography course (5 hours: R1500-00)</h1>
    How to correctly expose your photo: <br />
    <article>
        <span class="mod">MOD110-1:</span> The Triangle-of-Exposure. There are only three things you need to know... and each one has one desirable 'side-effect'<br />
        <span class="mod">MOD110-2:</span> More on f-stop numbers and how they relate to each other as well as their effect on depth-of-field<br />            
        <span class="mod">MOD110-3:</span> The Zone-system for determinig exposure
    </article>
    Photo composition: <br />
    <article>
        <span class="mod">MOD111-1:</span> Placing your subject using the rule-of-thirds and introducing the golden ratio and golden triangles<br />
        <span class="mod">MOD111-2:</span> Looking for further composition options using natural lines and frames around us 
    </article>
    Colour depth and colour spaces: <br />
    <article>
        <span class="mod">MOD112-1:</span> What is the RAW shooting format and why prefer it over jpg?<br />
        <span class="mod">MOD112-2:</span> What are colour spaces and what specifically are Adobe-RGB and SRGB?<br />
    </article>
    Camera Hardware: <br />
    <article>
        <span class="mod">MOD120-1:</span> How do lenses work? What are the advantages of prime lenses? What are the different focal lengths for?<br />
        <span class="mod">MOD120-2:</span> How important are megapixels really and why care about physical sensor size or it's crop factors?<br />            
        <span class="mod">MOD120-3:</span> What are lense filters? Discussing neutral density graduated (ND or NDG filters) and polarising filters.
    </article>
    <h1>CRS201: Post-processing your RAW photos</h1>
    Using Adobe Photoshop for RAW convertion: <br />
    <article>
        <span class="mod">MOD201-1:</span> Why do a RAW conversion and what is the DNG format?<br />
        <span class="mod">MOD201-2:</span> Doing a basic RAW conversion with Photoshop<br />
    </article>
</asp:Content>
