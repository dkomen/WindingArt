﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ph"] == "0")
        {
            Session.Add("stumbleupon", "1");
            Session.Remove("IgnorePageHits");
        }
        else if (Request.QueryString["ph"] == "1")
        {
            Session.Add("IgnorePageHits", "1");
            Session.Remove("stumbleupon");
        }


        if (Session["IgnorePageHits"] == null)
        {
            if (Session["stumbleupon"] != null)
            {
                PageFunctions.PageHitCounter(Request, "stumbleupon");
            }
            else
            {
                PageFunctions.PageHitCounter(Request);
            }
        }
        Response.Redirect("Galleries.aspx", true);
    }
}
