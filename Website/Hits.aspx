<%@ Page Language="C#" MasterPageFile="~/MasterPageDefault.master" AutoEventWireup="true" CodeFile="Hits.aspx.cs" Inherits="Hits" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="PageHitsTableAdapters.PageHitsTableAdapter">
        <SelectParameters>
            <asp:Parameter Name="PageHitId" Type="Int32" />
            <asp:Parameter Name="HitDateTime" Type="DateTime" />
            <asp:Parameter Name="PageId" Type="String" />
            <asp:Parameter Name="ReferrerIp" Type="String" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="HitDateTime" Type="DateTime" />
            <asp:Parameter Name="PageId" Type="String" />
            <asp:Parameter Name="ReferrerIp" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4"
        DataKeyNames="PageHitId" DataSourceID="ObjectDataSource1" ForeColor="#333333"
        GridLines="Vertical" AllowPaging="True" AllowSorting="True" 
    BorderStyle="Groove" PageSize="25">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="PageHitId" HeaderText="Id" InsertVisible="False"
                ReadOnly="True" SortExpression="PageHitId" >
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle Font-Size="Small" />
            </asp:BoundField>
            <asp:BoundField DataField="HitDateTime" HeaderText="Date Time" SortExpression="HitDateTime" >
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle Font-Size="Small" />
            </asp:BoundField>
            <asp:HyperLinkField HeaderText="Page" DataTextField="PageId" DataNavigateUrlFields="PageId"
            DataNavigateUrlFormatString="{0}" >
                <HeaderStyle HorizontalAlign="Left" Width="600px" /> 
                <ItemStyle Font-Size="Small" />
            </asp:HyperLinkField>
            <asp:BoundField DataField="ReferrerIp" HeaderText="Referrer IP" SortExpression="ReferrerIp" >
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle Font-Size="Small" />
            </asp:BoundField>
        </Columns>
        <RowStyle BackColor="#EFF3FB" Height="22px" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
</asp:Content>

