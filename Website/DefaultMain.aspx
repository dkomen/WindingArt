<%@ Page Language="C#" MasterPageFile="~/MasterPageDefault.master" AutoEventWireup="true" CodeFile="DefaultMain.aspx.cs" Inherits="_Default" Title="Dean Komen Photography" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script language="javascript" type="text/javascript">

     function SetImage(imageId, imageSource, categoryName, categoryElementID)
    {
        var imageElement = document.getElementById(imageId);
        imageElement.src=imageSource;        
        SetCatagory(categoryElementID, categoryName)
    }
    function NavigateTo(url)
    {
        window.location.href = url;
    }
    
    function SetCatagory(categoryElementID, catagoryName)
    {
        var catagoryElement = document.getElementById(categoryElementID);
        if(catagoryName!='')
        {
            catagoryElement.innerHTML= catagoryName;        
        }
        else
        {
            catagoryElement.innerHTML= "main galleries";        
        }
    }
    
    if (document.images) 
    {
        img1 = new Image();
        img2 = new Image();
        img3 = new Image();
        img4 = new Image();
        img5 = new Image();
        
        img1.src = "~/Images/MirrorOccasions.jpg";
        img2.src = "~/Images/MirrorLandscape.jpg"
        img3.src = "~/Images/MirrorNature.jpg"
        img4.src = "~/Images/MirrorArt.jpg"
        img5.src = "~/Images/WildlifeFineArt.jpg"
        
        img6 = new Image();
        img7 = new Image();
        img8 = new Image();
        img9 = new Image();
        img10 = new Image();
        img11 = new Image();
        img7.src = "~/Images/SubMenu/N1.jpg"
        img9.src = "~/Images/SubMenu/Sodwana.jpg"
        img10.src = "~/Images/SubMenu/Desktop.jpg"
    }

    
</script>
    <BR />
    <BR />
    <BR />
    <table style="width:750px;position:relative;top:-10px;margin-left: auto;margin-right: auto;" border="0">
        <tr>
            <td style="height:300px;width:3px;background-color:#777777"></td>
            <td valign='top'>
                <table border='0' cellpadding='10' cellspacing='0'>
                    <tr><td style="height:30px"></td></tr>
                    <tr>
                        <td>
                            <asp:image runat="server" id="MirrorMainPortfolio" ImageUrl="~/Images/MirrorMainPortfolioOut.jpg" ToolTip=""/>
                        </td>
                        <td>
                            <asp:image runat="server" id="MirrorNature" ImageUrl="~/Images/MirrorNatureOut.jpg" ToolTip=""/>
                        </td>
                        <td>
                            <asp:image runat="server" id="MirrorArt" ImageUrl="~/Images/MirrorArtOut.jpg" ToolTip=""/>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:15px;background-color:#000000" colspan='4'>
                            <asp:Label style="font-size:3;color:#555555" ID="uiCatagory" runat="server" Text="main galleries"></asp:Label>
                        </td>
                    </tr>
                    <tr><td style="height:30px">
                        </td></tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width:825px;position:relative;top:-10px;margin-left: auto;margin-right: auto;" border="0">
        <tr>
            <td align='right' style="width:400px"><hr style="background-color:Olive;color:Olive;"/>
                <asp:Label style="font-size:3;color:#555555" ID="uiAdditionalGal" runat="server" Text="additional galleries"></asp:Label>
            </td>
            <td align="left">
                <table border="0" cellspacing="20px">
                    <tr>
                        <td align="center" valign='top'>
                            <asp:image runat="server" id="HumanBody" ImageUrl="Images/SubMenu/HumanBodyOut.jpg" ToolTip=""/><br />
                            <asp:Label ID="Label3" Font-Size="Small" ForeColor="LightGray" Text="Human Body" runat="server" />
                        </td>
                        <td align="center" valign='top'>
                            <asp:image runat="server" id="N1" ImageUrl="Images/SubMenu/N1Out.jpg" ToolTip=""/><br />
                            <asp:Label ID="Label5" Font-Size="Small" ForeColor="LightGray" Text="N1<BR />highway" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    

<%--<img width="0" visible="false" height="0" border="0" alt="" src="http://mycounter.tinycounter.com/index.php?user=dkomen" />--%>
</asp:Content>

