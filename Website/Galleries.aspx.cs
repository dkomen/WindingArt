using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {      
	//PageFunctions.DoPageHitCounter(this);
	derotek.Utilities.PageHitCounter.Counter.AddHit("WindingArt",DateTime.Now,"/Pages/Galleries.aspx",Request.UserHostAddress);
        
        SetIconAttributes(MirrorWildlifePortfolio, "Pages/Wildlife/default.aspx", "wildlife portfolio");
        SetIconAttributes(MirrorLandscape, "Pages/Scapes/default.aspx", "land and seascapes portfolio");
        SetIconAttributes(MirrorNature, "Pages/Nature/default.aspx", "general nature");
        //SetIconAttributes(SchizoidReality, "Pages/SchizoidReality/default.aspx", "altered realities");

       
    }

    private void SetIconAttributes(Image imageIcon, string navigationUrl, string category)
    {
        imageIcon.Attributes.Add("onmouseover", "SetImage('" + imageIcon.ClientID + "', 'Images/" + imageIcon.ID + ".jpg', '" + category + "', '" + uiCatagory.ClientID + "')");
        imageIcon.Attributes.Add("onmouseout", "SetImage('" + imageIcon.ClientID + "', 'Images/" + imageIcon.ID + "Out.jpg', '', '" + uiCatagory.ClientID + "')");
        imageIcon.Attributes.Add("onclick", "NavigateTo('" + navigationUrl + "')");
    }
    private void SetIconAttributesSubMenu(Image imageIcon, string navigationUrl)
    {
        imageIcon.Attributes.Add("onmouseover", "SetImage('" + imageIcon.ClientID + "', 'Images/SubMenu/" + imageIcon.ID + ".jpg')");
        imageIcon.Attributes.Add("onmouseout", "SetImage('" + imageIcon.ClientID + "', 'Images/SubMenu/" + imageIcon.ID + "Out.jpg')");
        imageIcon.Attributes.Add("onclick", "NavigateTo('" + navigationUrl + "')");
    }
}
